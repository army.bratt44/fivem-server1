Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 0.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 206 }

Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- enable if you're using esx_identity
Config.EnableNonFreemodePeds      = true -- turn this on if you want custom peds
Config.EnableLicenses             = true -- enable if you're using esx_license

Config.EnableHandcuffTimer        = true -- enable handcuff timer? will unrestrain player after the time ends
Config.HandcuffTimer              = 10 * 60000 -- 10 mins

Config.EnableJobBlip              = true -- enable blips for colleagues, requires esx_society

Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.SandyStations = {

	LSPD = {

		Blip = {
			Coords  = vector3(1852, 3691, 35),
			Sprite  = 60,
			Display = 4,
			Scale   = 1.2,
			Colour  = 31
		},
					
		Cloakrooms = {
			vector3(1860, 3695, 34)
		},
		
		Armories = {
			vector3(1862, 3690, 34)
		},

		Vehicles = {
			{
				Spawner = vector3(1857, 3677, 34),
				InsideShop = vector3(228.5, -993.5, -99.5),
				SpawnPoints = {
					{ coords = vector3(1855, 3676, 34), heading = 207, radius = 6.0 },
					{ coords = vector3(1851, 3675, 34), heading = 207, radius = 6.0 },
					{ coords = vector3(453.5, -1022.2, 28.0), heading = 90.0, radius = 6.0 },
					{ coords = vector3(450.9, -1016.5, 28.1), heading = 90.0, radius = 6.0 }
				}
			},

			{
				Spawner = vector3(1857, 3677, 34),
				InsideShop = vector3(228.5, -993.5, -99.0),
				SpawnPoints = {
					{ coords = vector3(1855, 3676, 34), heading = 207.1, radius = 6.0 },
					{ coords = vector3(1851, 3675, 34), heading = 207.1, radius = 6.0 }
				}
			}
		},

		Helicopters = {
			{
				Spawner = vector3(1867, 3647, 34),
				InsideShop = vector3(477.0, -1106.4, 43.0),
				SpawnPoints = {
					{ coords = vector3(1871, 3646, 34), heading = 20.1, radius = 10.0 }
				}
			}
		},

		BossActions = {
			vector3(448.4, -973.2, 30.6)
		}

	}

}

Config.AuthorizedWeapons = {
	cadets = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 1500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 80 }
	},

	officer = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},
	
	Air_Unit = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 7000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},
	
	trooper = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 7000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	lieutenant = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 7000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	chief = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 7000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 }
	},

	boss = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 70000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
	},
	
	Sandys = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 70000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
	},
	
	Paleto = {
		{ weapon = 'WEAPON_PISTOL', components = { 1000, nil }, price = 1000 },
		{ weapon = 'WEAPON_ADVANCEDRIFLE', components = { 0, 6000, 1000, 4000, 8000, nil }, price = 5000 },
		{ weapon = 'WEAPON_PUMPSHOTGUN', components = { 2000, 6000, nil }, price = 70000 },
		{ weapon = 'WEAPON_NIGHTSTICK', price = 0 },
		{ weapon = 'WEAPON_STUNGUN', price = 500 },
		{ weapon = 'WEAPON_FLASHLIGHT', price = 0 },
	}
}

Config.AuthorizedVehicles = {
	Shared = {
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'pbus', label = 'Police Prison Bus', price = 1 },
		{ model = 'policeb', label = 'Police Bike', price = 1 },
		{ model = 'police3', label = 'Police Interceptor', price = 1 },
		{ model = 'policet', label = 'Police Transporter', price = 1 },
		{ model = 'riot', label = 'Police Riot', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 },
		{ model = 'policegl', label = 'Sheriff', price = 1 },
		{ model = 'policefelon', label = 'Unmarked car', price = 1 },
		{ model = 'chgr2', label = 'Paleto Bay Captian', price = 1 }
	},

	cadets = {
	
	},

	officer = {
		{ model = 'police3', label = 'Police Interceptor', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 }
	},

	sergeant = {
		{ model = 'policet', label = 'Police Transporter', price = 1 },
		{ model = 'policefelon', label = 'Unmarked Car', price = 1 }
	},

	trooper = {
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'policeb', label = 'Police Bike', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 }
	},

	lieutenant = {
		{ model = 'riot', label = 'Police Riot', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 },
		{ model = 'policegl', label = 'Sheriff', price = 1 },
		{ model = 'policefelon', label = 'Unmarked car', price = 1 }
	},

	chief = {
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'pbus', label = 'Police Prison Bus', price = 1 },
		{ model = 'policeb', label = 'Police Bike', price = 1 },
		{ model = 'police3', label = 'Police Interceptor', price = 1 },
		{ model = 'policet', label = 'Police Transporter', price = 1 },
		{ model = 'riot', label = 'Police Riot', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 },
		{ model = 'policegl', label = 'Sheriff', price = 1 },
		{ model = 'policefelon', label = 'Unmarked car', price = 1 }
	},

	boss = {
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'pbus', label = 'Police Prison Bus', price = 1 },
		{ model = 'policeb', label = 'Police Bike', price = 1 },
		{ model = 'police3', label = 'Police Interceptor', price = 1 },
		{ model = 'policet', label = 'Police Transporter', price = 1 },
		{ model = 'riot', label = 'Police Riot', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 },
		{ model = 'policegl', label = 'Sheriff', price = 1 },
		{ model = 'policefelon', label = 'Unmarked car', price = 1 },
		{ model = 'chgr2', label = 'Paleto Bay Captian', price = 1 }
	},
	Air_Unit = {
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'police3', label = 'Police Interceptor', price = 1 }
	},
	
	Sandys = {
		{ model = 'hcbr500rgn', label = 'Police Bike 500rgn', price = 1 },
		{ model = 'police', label = 'Police Cruiser', price = 1 },
		{ model = 'pbus', label = 'Police Prison Bus', price = 1 },
		{ model = 'policeb', label = 'Police Bike', price = 1 },
		{ model = 'police3', label = 'Police Interceptor', price = 1 },
		{ model = 'policet', label = 'Police Transporter', price = 1 },
		{ model = 'riot', label = 'Police Riot', price = 1 },
		{ model = 'fbi2', label = 'FIB SUV', price = 1 },
		{ model = 'pdram', label = 'Dogeram', price = 1 },
		{ model = 'wmfenyrcop', label = 'Highway Patrol', price = 1 },
		{ model = 'policegl', label = 'Sheriff', price = 1 },
		{ model = 'policefelon', label = 'Unmarked car', price = 1 }
	},
	
	Paleto = {
		{ model = 'policefelon', label = 'Unmarked car', price = 1 },
		{ model = 'chgr2', label = 'Paleto Bay Captian', price = 1 },
		{ model = 'pdram1', label = 'Paleto Bay Truck', price = 1 }
	}
}

Config.AuthorizedHelicopters = {
	cadets = {},

	officer = {},

	sergeant = {},

	trooper = {},

	lieutenant = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	},

	chief = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	},

	boss = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	},
	
	Air_Unit = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	},
	
	Sandys = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	},
	
	Paleto = {
		{ model = 'polmav', label = 'Police Maverick', livery = 0, price = 1 }
	}
}

-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
	cadets_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 46,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = 45,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	officer_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 0,   ['decals_2'] = 0,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	sergeant_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 1,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 1,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	trooper_wear = {
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 2,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 2,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	lieutenant_wear = { -- currently the same as intendent_wear
		male = {
			['tshirt_1'] = 58,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 2,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = 1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 35,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 7,   ['decals_2'] = 2,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	chief_wear = {
		male = {
			['tshirt_1'] = 122,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 122,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	boss_wear = { -- currently the same as chef_wear
		male = {
			['tshirt_1'] = 122,  ['tshirt_2'] = 0,
			['torso_1'] = 55,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 41,
			['pants_1'] = 25,   ['pants_2'] = 0,
			['shoes_1'] = 25,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		},
		female = {
			['tshirt_1'] = 122,  ['tshirt_2'] = 0,
			['torso_1'] = 48,   ['torso_2'] = 0,
			['decals_1'] = 8,   ['decals_2'] = 3,
			['arms'] = 44,
			['pants_1'] = 34,   ['pants_2'] = 0,
			['shoes_1'] = 27,   ['shoes_2'] = 0,
			['helmet_1'] = -1,  ['helmet_2'] = 0,
			['chain_1'] = 0,    ['chain_2'] = 0,
			['ears_1'] = 2,     ['ears_2'] = 0
		}
	},
	bullet_wear = {
		male = {
			['bproof_1'] = 11,  ['bproof_2'] = 1
		},
		female = {
			['bproof_1'] = 13,  ['bproof_2'] = 1
		}
	},
	gilet_wear = {
		male = {
			['tshirt_1'] = 59,  ['tshirt_2'] = 1
		},
		female = {
			['tshirt_1'] = 36,  ['tshirt_2'] = 1
		}
	}

}