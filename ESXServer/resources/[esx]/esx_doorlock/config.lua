Config = {}
Config.Locale = 'en'

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- Entrance Doors
	{
		textCoords = vector3(434.7, -982.0, 31.5),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = false,
		distance = 2.5,
		doors = {
			{
				objName = 'v_ilev_ph_door01',
				objYaw = -90.0,
				objCoords = vector3(434.7, -980.6, 30.8)
			},


			{
				objName = 'v_ilev_ph_door002',
				objYaw = -90.0,
				objCoords = vector3(434.7, -983.2, 30.8)
			}
		}
	},

	-- To locker room & roof
	{
		objName = 'v_ilev_ph_gendoor004',
		objYaw = 90.0,
		objCoords  = vector3(449.6, -986.4, 30.6),
		textCoords = vector3(450.1, -986.3, 31.7),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Rooftop
	{
		objName = 'v_ilev_gtdoor02',
		objYaw = 90.0,
		objCoords  = vector3(464.3, -984.6, 43.8),
		textCoords = vector3(464.3, -984.0, 44.8),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Hallway to roof
	{
		objName = 'v_ilev_arm_secdoor',
		objYaw = 90.0,
		objCoords  = vector3(461.2, -985.3, 30.8),
		textCoords = vector3(461.5, -986.0, 31.5),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Armory
	{
		objName = 'v_ilev_arm_secdoor',
		objYaw = -90.0,
		objCoords  = vector3(452.6, -982.7, 30.6),
		textCoords = vector3(453.0, -982.6, 31.7),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Captain Office
	{
		objName = 'v_ilev_ph_gendoor002',
		objYaw = -180.0,
		objCoords  = vector3(447.2, -980.6, 30.6),
		textCoords = vector3(447.2, -980.0, 31.7),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- To downstairs (double doors)
	{
		textCoords = vector3(444.6, -989.4, 31.7),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_ph_gendoor005',
				objYaw = 180.0,
				objCoords = vector3(443.9, -989.0, 30.6)
			},

			{
				objName = 'v_ilev_ph_gendoor005',
				objYaw = 0.0,
				objCoords = vector3(445.3, -988.7, 30.6)
			}
		}
	},

	--
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 0.0,
		objCoords  = vector3(463.8, -992.6, 24.9),
		textCoords = vector3(463.3, -992.6, 25.1),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -90.0,
		objCoords  = vector3(462.3, -993.6, 24.9),
		textCoords = vector3(461.8, -993.3, 25.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 90.0,
		objCoords  = vector3(462.3, -998.1, 24.9),
		textCoords = vector3(461.8, -998.8, 25.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 90.0,
		objCoords  = vector3(462.7, -1001.9, 24.9),
		textCoords = vector3(461.8, -1002.4, 25.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	-- To Back
	{
		objName = 'v_ilev_gtdoor',
		objYaw = 0.0,
		objCoords  = vector3(463.4, -1003.5, 25.0),
		textCoords = vector3(464.0, -1003.5, 25.5),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	--
	-- Mission Row Back
	--

	-- Back (double doors)
	{
		textCoords = vector3(468.6, -1014.4, 27.1),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_rc_door2',
				objYaw = 0.0,
				objCoords  = vector3(467.3, -1014.4, 26.5),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			},

			{
				objName = 'v_ilev_rc_door2',
				objYaw = 180.0,
				objCoords  = vector3(469.9, -1014.4, 26.5),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			}
		}
	},

	-- Back Gate
	{
		objName = 'hei_prop_station_gate',
		objYaw = 90.0,
		objCoords  = vector3(488.8, -1017.2, 27.1),
		textCoords = vector3(488.8, -1020.2, 30.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 14,
		size = 2
	},

	--
	-- Sandy Shores
	--

	-- Entrance
	{
		objName = 'v_ilev_shrfdoor',
		objYaw = 30.0,
		objCoords  = vector3(1855.1, 3683.5, 34.2),
		textCoords = vector3(1855.1, 3683.5, 35.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = false
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -60.0,
		objCoords  = vector3(1848, 3683, 34),
		textCoords = vector3(1847, 3682, 35),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -60.0,
		objCoords  = vector3(1847, 3686, 34),
		textCoords = vector3(1846, 3685, 35),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 120.0,
		objCoords  = vector3(1845, 3688, 34),
		textCoords = vector3(1844, 3687, 35),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	-- Back (double doors)
	{
		textCoords = vector3(1848, 3691, 35),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_rc_door2',
				objYaw = 30.0,
				objCoords  = vector3(1848, 3691, 34),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			},

			{
				objName = 'v_ilev_rc_door2',
				objYaw = 210.0,
				objCoords  = vector3(1849, 3692, 34),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			}
		}
		
	},
	
	-- Left (double doors)
	{
		textCoords = vector3(1851, 3683, 35),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 4,
		doors = {
			{
				objName = 'v_ilev_rc_door2',
				objYaw = 118.8,
				objCoords  = vector3(1851, 3683, 34),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			},

			{
				objName = 'v_ilev_rc_door2',
				objYaw = -59.9,
				objCoords  = vector3(1851, 3684, 34),
				authorizedJobs = { 'police', 'sandy', 'paleto' },
				locked = true
			}
		}
	},
	
	-- Locker Room
	{
		objName = 'v_ilev_rc_door2',
		objYaw = 30.0,
		objCoords  = vector3(1857, 3690, 34),
		textCoords = vector3(1856, 3691, 34),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	--
	-- Paleto Bay
	--

	-- Entrance (double doors)
	{
		textCoords = vector3(-443.5, 6016.3, 32.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = false,
		distance = 2.5,
		doors = {
			{
				objName = 'v_ilev_shrf2door',
				objYaw = -45.0,
				objCoords  = vector3(-443.1, 6015.6, 31.7),
			},

			{
				objName = 'v_ilev_shrf2door',
				objYaw = 135.0,
				objCoords  = vector3(-443.9, 6016.6, 31.7)
			}
		}
	},
	
	-- Main CellDoor
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-432, 5993, 32),
		textCoords = vector3(-433, 5993, 32),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	-- CellDoor 1
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-428, 5997, 32),
		textCoords = vector3(-427, 5996, 32),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},
	
	-- CellDoor 2
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = -45.0,
		objCoords  = vector3(-431, 6000, 32),
		textCoords = vector3(-430, 5999, 32),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true
	},

	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objName = 'prop_gate_prison_01',
		objCoords  = vector3(1844.9, 2604.8, 44.6),
		textCoords = vector3(1844.9, 2608.5, 48.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 12,
		size = 2
	},

	{
		objName = 'prop_gate_prison_01',
		objCoords  = vector3(1818.5, 2604.8, 44.6),
		textCoords = vector3(1818.5, 2608.4, 48.0),
		authorizedJobs = { 'police', 'sandy', 'paleto' },
		locked = true,
		distance = 12,
		size = 2
	},
	
}