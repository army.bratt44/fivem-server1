USE `essentialmode`;

INSERT INTO `addon_account` (name, label, shared) VALUES
	('society_paleto', 'Paleto', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	('society_paleto', 'Paleto', 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	('society_paleto', 'Paleto', 1)
;

INSERT INTO `jobs` (name, label) VALUES
	('paleto','PBPD')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('paleto',0,'recruit','Recrue',1000,'{}','{}'),
	('paleto',1,'officer','Officier',1500,'{}','{}'),
	('paleto',2,'sergeant','Sergent',2000,'{}','{}'),
	('paleto',3,'lieutenant','Lieutenant',2500,'{}','{}'),
	('paleto',4,'boss','Commandant',3000,'{}','{}')
;