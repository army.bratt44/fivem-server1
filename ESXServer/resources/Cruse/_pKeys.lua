--[[
-----------------------------------------------------------------------------------------------------------------------------
README:		(do not read unless you are a lua scripter)
-----------------------------------------------------------------------------------------------------------------------------

	this script relies on the keys.lua which comes with the current version of LUA Plugin for Script Hook V (v10.1)

	unlike get_key_pressed(Keys.key) these functions use string input which is then associated with the Keys table

	examples:	local key = "RControlKey"
				GetKeyPressed(key)
				GetKeyJustReleased("F")
				GetKeyJustPressed("Return")
				GetKeyReleased("LShiftKey")

	using get_key_pressed(Keys.key) instead of GetKeyPressed(key) can cause some problems to occur if used in the same script

-----------------------------------------------------------------------------------------------------------------------------
FUNCTIONS AND THEIR DESCRIPTION
-----------------------------------------------------------------------------------------------------------------------------

	GetKeyJustPressed(key)	::	returns true if the key has been just pressed

	GetKeyJustReleased(key)	::	returns true if the key has been just released

	GetKeyPressed(key)		::	returns true if the key is pressed

	GetKeyReleased(key)		::	returns true if the key is released

	GetKeyStatus(key)		::	returns string containing status of the key, this is just for testing

-----------------------------------------------------------------------------------------------------------------------------
]]


local pKeys = {[65535] = false, [65536] = false, [131072] = false, [262144] = false, [-65536] = false}

for i = 0, 254 do
	pKeys[i] = false
end

function GetKeyJustPressed(key)
	if get_key_pressed(Keys[key]) then
		if pKeys[Keys[key]] == false then
			pKeys[Keys[key]] = true
			return true
		end
	elseif pKeys[Keys[key]] == true then
		pKeys[Keys[key]] = false
	end
end

function GetKeyJustReleased(key)
	if get_key_pressed(Keys[key]) then
		if pKeys[Keys[key]] == false then
			pKeys[Keys[key]] = true
		end
	elseif pKeys[Keys[key]] == true then
		pKeys[Keys[key]] = false
		return true
	end
end

function GetKeyPressed(key)
	if get_key_pressed(Keys[key]) then
		if pKeys[Keys[key]] == false then
			pKeys[Keys[key]] = true
		else
			return true
		end
	elseif pKeys[Keys[key]] == true then
		pKeys[Keys[key]] = false
	end
end

function GetKeyReleased(key)
	if get_key_pressed(Keys[key]) then
		if pKeys[Keys[key]] == false then
			pKeys[Keys[key]] = true
		end
	elseif pKeys[Keys[key]] == true then
		pKeys[Keys[key]] = false
	else
		return true
	end
end

function GetKeyStatus(key)
	if get_key_pressed(Keys[key]) then
		if pKeys[Keys[key]] == false then
			pKeys[Keys[key]] = true
			return "key "..key.." just pressed"
		else
			return "key "..key.." pressed"
		end
	elseif pKeys[Keys[key]] == true then
		pKeys[Keys[key]] = false
		return "key "..key.." just released"
	else
		return "key "..key.." released"
	end
end


local _pKeys = {}
--[[
function _pKeys.init()
end

function _pKeys.tick()
end

function _pKeys.unload()
end
]]
return _pKeys

