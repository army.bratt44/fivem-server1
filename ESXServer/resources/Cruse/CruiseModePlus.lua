

-- if you are unsure of what the key that you wish to use is called see the "Grand Theft Auto V\scripts\keys.lua" for the full list


-- the key to open cruise menu and exit the cruise mode
local key_toggle = "K"

-- keys to navigate the cruise mode menu
local key_menu_up = "W"
local key_menu_down = "S"
local key_menu_select = "E"
local key_menu_close = "K"

-- the key to disable cruise mode in case of emergency
local key_disable = "Space"

-- the key to incerase the cruise speed
local key_forward = "W"		-- should be the accelerate key

-- the key to decrease the cruise speed
local key_reverse = "S"		-- should be the brake/reverse key

-- fly mode controls only
local key_elevate = "Q"		-- up like a heli
local key_descend = "Z"		-- down like a heli
local key_yaw_l = "A"		-- left turn like a car
local key_yaw_r = "D"		-- right turn like a car
local key_roll_l = "Left"	-- do a barrel roll left
local key_roll_r = "Right"	-- do a barrel roll right
local key_pitch_down = "Up"	-- nose dive
local key_pitch_up = "Down"	-- the oposite of previous

-- normal mode feature only, triggers if the car is not touching the ground with all wheels
-- 0: disabled
-- 1: car is forced to stick to the ground (recommended)
local magnets = 1
-- this is here to prevent your car spontaneously taking off

-- normal mode feature only, triggers if the car is not touching the ground with all wheels
-- 0: disabled
-- 1: stop moving (recommended)
-- 2: stop moving and disable cruise mode
local emergencystop = 1
-- this is here to prevent gettin yourself murdered in a car on building sumo match

-- fly mode feature only
-- 0: disabled
-- 1: enabled
local wind_enabled = 1


----------------------------------------------------------------------------------------------------


local mode = 1
local speed = 0
local vel = 0
local vel_x = 0
local vel_y = 0
local vel_z = 0
local wind = 0
local wind_x_stage = 0
local wind_x_max = 2
local wind_x_half = wind_x_max/2
local wind_y_stage = 0
local wind_y_max = 1
local wind_y_half = wind_y_max/2
local wind_z_stage = 0
local wind_z_max = 3
local wind_z_half = wind_z_max/2
local wind_dir_x = 1
local wind_dir_y = 0
local wind_dir_z = 1
local enabled = false


local text_timer = 0
local texttoshow = "NULL"
local function showtext(text)
	text_timer = 50
	texttoshow = text
end
local function show_text()
	UI.SET_TEXT_FONT(1)
	UI.SET_TEXT_SCALE(0.6, 0.8)
	UI.SET_TEXT_COLOUR(255, 255, 155, 255)
	UI.SET_TEXT_CENTRE(true)
	UI.SET_TEXT_DROPSHADOW(20, 20, 0, 0, 0)
	UI._SET_TEXT_ENTRY("STRING")
	UI._ADD_TEXT_COMPONENT_STRING(texttoshow)
	UI._DRAW_TEXT(0.5, 0.8)
	text_timer = text_timer-1
end
local function show_menu(g,m,p)
	UI.SET_TEXT_FONT(1)
	UI.SET_TEXT_SCALE(0.6, 0.8)
	UI.SET_TEXT_COLOUR(155, g, 155, 255)
	UI.SET_TEXT_DROPSHADOW(20, 20, 0, 0, 0)
	UI._SET_TEXT_ENTRY("STRING")
	UI._ADD_TEXT_COMPONENT_STRING("Cruise Mode: "..m)
	UI._DRAW_TEXT(0.4, 0.6 + p)
end
local menu_open = false


local CruiseModePlus = {} 


function CruiseModePlus.init()

end


function CruiseModePlus.tick()

	if text_timer > 0 then
		show_text()
	end

	if menu_open then
		if mode == 1 then show_menu(255, "NORMAL", 0) else show_menu(155, "Normal", 0) end
		if mode == 2 then show_menu(255, "FLYING", 0.05) else show_menu(155, "Flying", 0.05) end
		if mode == 3 then show_menu(255, "UNDERWATER", 0.1) else show_menu(155, "Underwater", 0.1) end
		if GetKeyJustReleased(key_menu_select) then
			if mode == 2 then
				VEHICLE.SET_VEHICLE_GRAVITY(PED.GET_VEHICLE_PED_IS_USING(PLAYER.PLAYER_PED_ID()), false)
			end
			menu_open = false
			enabled = true
			showtext("Cruise Mode: ON")
		elseif GetKeyJustPressed(key_menu_close) then
			menu_open = false
		elseif GetKeyJustPressed(key_menu_up) then
			if mode > 1 then
				mode = mode - 1
			else
				mode = 3
			end
		elseif GetKeyJustPressed(key_menu_down) then
			if mode < 3 then
				mode = mode + 1
			else
				mode = 1
			end
		end
	elseif enabled then
		local player = PLAYER.PLAYER_PED_ID()
		if ENTITY.DOES_ENTITY_EXIST(player) == true then
			if PED.IS_PED_IN_ANY_VEHICLE(player, false) == true then
				local vehicle = PED.GET_VEHICLE_PED_IS_USING(player)
				if mode == 1 then

					if not VEHICLE.IS_VEHICLE_ON_ALL_WHEELS(vehicle) then
						if magnets > 0 then
							VEHICLE.SET_VEHICLE_ON_GROUND_PROPERLY(vehicle)
						end
						if emergencystop > 0 then
							if emergencystop == 1 then
								speed = 0
								VEHICLE.SET_VEHICLE_FORWARD_SPEED(vehicle,speed)
							else
								speed = 0
								enabled = false
								showtext("Cruise Mode: OFF")
							end
							return
						end
					end

					if GetKeyPressed(key_forward) or GetKeyPressed(key_reverse) then
						speed = ENTITY.GET_ENTITY_SPEED_VECTOR(vehicle, true)
						speed = speed.y
						showtext("Cruise Speed: "..math.ceil(speed*10)/10)
					else
						VEHICLE.SET_VEHICLE_FORWARD_SPEED(vehicle,speed)
					end

				elseif mode == 2 then

					local rot = ENTITY.GET_ENTITY_ROTATION(vehicle, false)
					if GetKeyPressed(key_pitch_up) then
						rot.x=rot.x+2
					elseif GetKeyPressed(key_pitch_down) then
						rot.x=rot.x-2
					end
					if GetKeyPressed(key_yaw_l) then
						rot.z=rot.z+2
					elseif GetKeyPressed(key_yaw_r) then
						rot.z=rot.z-2
					end
					if GetKeyPressed(key_roll_l) then
						if rot.y > -88 then
							rot.y=rot.y-2
						end
					elseif GetKeyPressed(key_roll_r) then
						if rot.y < 88 then
							rot.y=rot.y+2
						end
					end
					ENTITY.SET_ENTITY_ROTATION(vehicle, rot.x, rot.y, rot.z, 0, false)

					speed = ENTITY.GET_ENTITY_SPEED_VECTOR(vehicle, true)
					speed = speed.y
					if GetKeyPressed(key_forward) then
						speed = speed + 5
					elseif GetKeyPressed(key_reverse) then
						speed = speed - 10
					else
						if speed ~= 0 then
							if speed > 1 then
								speed = speed - 1
							elseif speed < -1 then
								speed = speed + 1
							else
								speed = 0
							end
						end
					end
					VEHICLE.SET_VEHICLE_FORWARD_SPEED(vehicle,speed)

					vel = ENTITY.GET_ENTITY_VELOCITY(vehicle)
					if wind_enabled > 0 then
						if wind_x_stage<wind_x_max then wind_x_stage=wind_x_stage+0.01 else wind_x_stage=0 wind_x_max=(6+math.random(24))/10 wind_x_half=wind_x_max/2 if wind_dir_x == 1 then wind_dir_x = 0 else wind_dir_x = 1 end end
						if wind_y_stage<wind_y_max then wind_y_stage=wind_y_stage+0.01 else wind_y_stage=0 wind_y_max=(6+math.random(24))/10 wind_y_half=wind_y_max/2 if wind_dir_y == 1 then wind_dir_y = 0 else wind_dir_y = 1 end end
						if wind_z_stage<wind_z_max then wind_z_stage=wind_z_stage+0.01 else wind_z_stage=0 wind_z_max=(6+math.random(24))/10 wind_z_half=wind_z_max/2 if wind_dir_z == 1 then wind_dir_z = 0 else wind_dir_z = 1 end end
						wind = GAMEPLAY.GET_WIND_DIRECTION()
						if wind_dir_x==1 then vel_x=vel.x+(wind.x*(wind_x_stage-wind_x_half))else vel_x=vel.x-(wind.x*(wind_x_stage-wind_x_half))end
						if wind_dir_y==1 then vel_y=vel.y+(wind.y*(wind_y_stage-wind_y_half))else vel_y=vel.y-(wind.y*(wind_y_stage-wind_y_half))end
						if wind_dir_z==1 then vel_z=vel.z+(-1*(wind_z_stage-wind_z_half))else vel_z=vel.z-(-1*(wind_z_stage-wind_z_half))end
						ENTITY.SET_ENTITY_VELOCITY(vehicle,vel_x,vel_y,vel_z)
					end
					if GetKeyPressed(key_elevate) then
						ENTITY.SET_ENTITY_VELOCITY(vehicle, vel.x, vel.y, vel.z+10)
					elseif GetKeyPressed(key_descend) then
						ENTITY.SET_ENTITY_VELOCITY(vehicle, vel.x, vel.y, vel.z-10)
					end

				elseif mode == 3 then
					VEHICLE.SET_VEHICLE_ENGINE_ON(vehicle, true, true)
					VEHICLE.SET_VEHICLE_ON_GROUND_PROPERLY(vehicle)
					speed = ENTITY.GET_ENTITY_SPEED_VECTOR(vehicle, true)
					speed = speed.y
					if GetKeyPressed(key_forward) then
						speed = speed + 0.6
					elseif GetKeyPressed(key_reverse) then
						speed = speed - 0.6
					else
						if speed ~= 0 then
							if speed > 0.3 then
								speed = speed - 0.3
							elseif speed < - 0.3 then
								speed = speed + 0.3
							else
								speed = 0
							end
						end
					end
					VEHICLE.SET_VEHICLE_FORWARD_SPEED(vehicle,speed)
				end

				if GetKeyJustReleased(key_toggle) or GetKeyJustReleased(key_disable) then
					if mode == 2 then
						VEHICLE.SET_VEHICLE_GRAVITY(vehicle, true)
					end
					speed = 0
					enabled = false
					showtext("Cruise Mode: OFF")
				end
			else
				if mode == 2 then
					VEHICLE.SET_VEHICLE_GRAVITY(PED.GET_VEHICLE_PED_IS_USING(player), true)
				end
				speed = 0
				enabled = false
				showtext("Cruise Mode: OFF")
			end
		end
	elseif GetKeyJustReleased(key_toggle) then
		local player = PLAYER.PLAYER_PED_ID()
		if PED.IS_PED_IN_ANY_VEHICLE(player, false) == true then
			menu_open = true
		end
	end
end


function CruiseModePlus.unload()

end


return CruiseModePlus

